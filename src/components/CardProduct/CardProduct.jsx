import React, { useContext } from 'react';
import {Link} from 'react-router-dom'
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';
import { ContextData } from '../../context/DataContext';

import './CardProduct.css'

const CardProduct = ({image, price,title,id,description}) => {
  const {addToCart} = useContext(ContextData)
  const product = {image, price,title,id,description}
  
  return (
    <div className="product-container">
      <Card className="product-card">
        <Link to={`/product-detail/${id}`} >
          <img className="card-image" top width="100%" src={image} alt="Card image cap" />        
        </Link>
        <CardBody>
          <CardTitle tag="h5">{title.substr(0,40)}</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">{price} $</CardSubtitle>
          <CardText>{description.substr(0,100)}</CardText>
        </CardBody>
          <button className="bttn-buy" onClick={()=>addToCart(product)}>Add to cart</button>
      </Card>
    </div>
  );
};

export default CardProduct;