import React, { useContext } from 'react';
import './Products.css'
import { ContextData } from '../../context/DataContext';
import CardProduct from '../../components/CardProduct/CardProduct'

function Products(){
    const context = useContext(ContextData)
    const productList = context.data

    return(
        <div className="products-container">
            {productList && productList.map(item => {
                const {image, price,title,id,description,category} = item;
                return <CardProduct image={image} price={price} title={title} key={id} id={id} description={description} category={category} />
            })}
        </div>
    )
}

export default Products