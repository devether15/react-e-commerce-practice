import React, {Fragment, useContext} from 'react'
import { ContextData } from '../../context/DataContext';
import './Cart.css'

function Cart(props){
    const {cartItem,
         increaseCartQuantity,
         decreaseCartQuantity,
         removeFromCart,
         calulateTotal } = useContext(ContextData)
      
    return(         
        <div className='cart-container'>
            <div className="product-cart-row-titles">
                <div>Image</div>
                <div>Title</div>                
                <div>Quantity</div>
                <div></div>
                <div>Price</div>
                <div>Total</div>
            </div>
            {cartItem && cartItem.map(item => {
            const {image, title, price, quantity} = item
           
            return <div className="product-cart-container">
                        <div className='cart-image-container'>
                            <img src={image} alt=""/>
                        </div>
                        <div className='cart-title'>{title}</div>                                               
                        <div class="product-quantity">
                            <button className="addBttn" onClick={()=>decreaseCartQuantity(item)}>-</button>
                            <input type="text" value={quantity}/>
                            <button className="decreaseBtnn" onClick={()=>increaseCartQuantity(item)}>+</button>
                        </div>
                        <div><button onClick={()=>removeFromCart(item)}>Remove</button></div>
                        <div className='cart-price'>{price} $</div>
                        <div className='cart-total'>{calulateTotal(price, quantity)} $</div>
                    </div>
            })}
        </div>    
    )
}

export default Cart