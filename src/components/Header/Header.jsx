import React from 'react';
import './Header.css'
import {Link} from 'react-router-dom'

function Header(){
    return(        
        <nav className="header-container">
            <div>
                logo
            </div>
            <ul>
                <li>Categories</li>
                <li>Login</li>
                <Link to="/cart">
                    <li>Cart</li>
                </Link>
            </ul>
        </nav>    
    )
}

export default Header