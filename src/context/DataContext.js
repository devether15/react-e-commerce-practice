import React, {useState}  from 'react'
import {useFetch} from '../hooks/useFetch'

export const ContextData = React.createContext(null)

export const ContextDataProvider = (props) => {
   
    const {data, loading, error} = useFetch()
    
    const [ cartItem, setCartItem] = useState([])

    React.useEffect(() => {
        const dataCart = localStorage.getItem('cart-item')
        if(dataCart){
            setCartItem(JSON.parse(dataCart))
        }
    }, [])

    React.useEffect(() => {
        localStorage.setItem("cart-item", JSON.stringify(cartItem));
    })  

    const addToCart = (item) =>{  
             
        setCartItem(currentState =>  [...currentState, {...item, quantity: item.quantity = 1}])  
    }

    const removeFromCart =(product) =>{
        setCartItem(currentCartItem =>{
            return currentCartItem.filter(item => item.id !== product.id)
                                 
        })
    }

    const increaseCartQuantity = (product) =>{                
        setCartItem(currentCartItem => {
            const existing = cartItem.find(item => item.id === product.id)
            return existing
            ? [
                ...cartItem.map(item =>
                item.id === product.id
                    ? { ...item, quantity: item.quantity + 1 }
                    : item,
                ),
            ]
            : [...currentCartItem, { ...product, quantity: 1 }];           
        })    
    }

    const decreaseCartQuantity = (product) => { 
          
        if (product.quantity < 2 ) return removeFromCart(product);
        
        setCartItem((currentCartItem) => [
            ...currentCartItem.map((item) =>
              item.id === product.id ? { ...item, quantity: item.quantity - 1 } : item,
            ),
        ]);
    }

    const calulateTotal = (price, quantity) =>{
        return price * quantity
    }

    return (
        <ContextData.Provider value={
            {data,
             loading,
             error,
             addToCart,
             cartItem,
             increaseCartQuantity,
             decreaseCartQuantity,
             removeFromCart,
             calulateTotal
             }
        }>
            {props.children}
        </ContextData.Provider>    
    )
}