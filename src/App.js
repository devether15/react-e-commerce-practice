import './App.css';
import Home from './pages/Home/Home'
import { BrowserRouter, Switch, Route} from 'react-router-dom' 
import Header from './components/Header/Header'
import Footer from './components/footer/Footer'
import { ContextDataProvider } from './context/DataContext';
import ProductDetail from './pages/ProductDetail/ProductDetail';
import Checkout from './pages/checkout/checkout';
import Cart from './components/Cart/Cart';

function App() {
  return (
    <BrowserRouter>
      <ContextDataProvider>    
        <div>
          <Header />
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path="/product-detail/:id" component={ProductDetail} />
            <Route path="/cart" component={Cart} />      
          </Switch>
          {/* <Footer /> */}
        </div>
      </ContextDataProvider>
    </BrowserRouter>  
  );
}

export default App;
