import React from 'react';
import './Home.css'
import Products from '../../components/Products/Products'

function Home(){
    return(
        <div className="home-container">
            <Products />
        </div>
    )
}

export default Home