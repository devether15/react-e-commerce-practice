import React from 'react'
import { useParams } from 'react-router'
import { useFetchDetail } from '../../hooks/useFetchDetail'
import './ProductDetail.css'


function ProductDetail (props){
    // console.log(props.match.params.id)
    const {id} = useParams()
    const {data} = useFetchDetail(id)
    console.log(data)
    const {image, price,title, description,category} = data
//    const {}
    return(
        <div className='product-detail-container'>
            <div className="image-container">
                <img src={image} alt=""/>
            </div>
            <div className="description-container">
                <div className="title-container">
                    <h1 className="title">{title}</h1>
                    <div className="category">{category}</div>
                </div>
                <p className="description">{description}</p>
                
                <div className="price">{price} USD$</div>
                <button className="bttn-buy">Add to cart</button>
            </div>
        </div>
    )

}

export default ProductDetail