import React, {useState, useEffect} from 'react'

export const useFetchDetail = (id) => {

    const urlProducts = `https://fakestoreapi.com/products/${id}`

    const [state,setState] = useState({data: {}, loading:true, error:''})

    useEffect(() => {
        const fetchData = async () =>{
            const response = await fetch(urlProducts)
            const data = await response.json()
            setState({data: data, loading: false, error: null})
        }

        fetchData()
        
    }, [])
     
// console.log(state);
    return state

}